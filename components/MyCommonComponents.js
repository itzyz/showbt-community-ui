import {Checkbox, Avatar, Card, List, Modal, Form, Space, Input, message, Button} from "antd";
import React, {useState} from "react";
import dynamic from "next/dynamic";
import {EyeOutlined, LikeOutlined, MessageOutlined, TagsFilled} from "@ant-design/icons";
import utilStyles from "../styles/utils.module.css";
import {Config} from "../utils/config";
import {get, post} from "../utils/request";

import moment from "moment";
import {getHashCode} from "../utils/tools";
import {useRouter} from "next/router";

//自定义icon
export const IconText = ({icon, text}) => (
    <Space>
        {icon ? React.createElement(icon) : null}
        {text}
    </Space>
);

//动态渲染Tag,解决next.js加载antd的Tag组件报错问题
export const MyTag = dynamic(() => {
    return import("antd").then(mod => mod.Tag);
}, {ssr: false})

export const LongToDate = (l) => {
    return moment(l).format('YYYY-MM-DD HH:mm:ss');
}
// const router = useRouter()
//问题列表展示
export const QuestionList = ({data, router}) => {
    {
        // const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
        return data ? <List
            itemLayout="vertical"
            size="large"
            dataSource={data['records']}
            renderItem={item => (
                <List.Item
                    key={item.id}
                    actions={[
                        <IconText icon={EyeOutlined} text={item['viewCount']}
                                  key="list-vertical-star-o"/>,
                        <IconText icon={LikeOutlined} text={item['likeCount']}
                                  key="list-vertical-like-o"/>,
                        <IconText icon={MessageOutlined} text={item['commentCount']}
                                  key="list-vertical-message"/>,
                        <IconText text={LongToDate(item['gmtCreate'])} key="list-vertical-time"/>,
                    ]}
                    extra={
                        item.pic ?
                            <img
                                width={272}
                                alt="logo"
                                src={item.pic}
                            /> : null
                    }
                >
                    <List.Item.Meta
                        avatar={<Avatar src={item.user['avatarUrl']}/>}
                        title={<a
                            onClick={() => {
                                const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                router.push({
                                    pathname: "/question/" + item.id,
                                    query: {
                                        sessionId: sid
                                    }
                                }).then()
                            }}
                            // href={"/question/" + item.id + "?sessionId=" + sid}
                        >{item.title}</a>}
                        description={item.title}
                    />
                    {/*{item.title}*/}
                </List.Item>
            )}
        /> : <div>这家伙很懒，什么都没干过！</div>
    }

}

//关注人员列表
export const CollectionUsers = ({data}) => {
    const users = data ? data : [];
    return <Card title={users.length + "人关注"} bordered={false}>
        {users.map((u, index) => {
            return <Avatar key={index} shape="square" src={u['avatarUrl']}
                           style={{margin: "5px 5px"}}/>
        })}
    </Card>
}

//标签列表展示
export const TagsShow = ({title, data}) => {
    const tags = data ? data : [];
    return <Card title={<IconText icon={() => <TagsFilled style={{color: "#f40", fontSize: "20px"}}/>}
                                  text={title}/>} bordered={false}>
        <ul className={utilStyles.tags}>
            {tags.map((r, index) => {
                // let num = Math.floor(Math.random() * index);
                let num = getHashCode(r)
                return <li key={index}>
                    <MyTag color={Config.COLOR_DATA[num % 10]}>{r}</MyTag>
                </li>
            })}
        </ul>
    </Card>
}

//登录表单
export const LoginForm = ({visible, handleOk}) => {
    const [form] = Form.useForm();
    const [imageUrl, setImageUrl] = useState(null)
    const [randomCode, setRandomCode] = useState(null)

    const getValidateCode = () => {
        get("/api/code").then(res => {
            if (res.data.code === 200) {
                setImageUrl(res.data.data.image)
                setRandomCode(res.data.data['validateKey'])
            }
        }).catch(err => {
            message.destroy();
            message.error("获取验证码失败！").then()
        })
    }
    const onFinish = () => {
        form.validateFields().then(values => {
            values.randomCode = randomCode;
            post("/api/login", values).then(r => {
                const user = r.data.data;
                if (user.key) {
                    sessionStorage.setItem(Config.SESSION_USER_KEY, user.key);
                    sessionStorage.setItem(Config.SESSION_USER_INFO, JSON.stringify(user.user));
                    handleOk(true);
                } else {
                    message.destroy();
                    message.error("错误信息：").then()
                }
            }).catch(e => {
                message.destroy();
                message.error("登录失败！").then()
            })
        })
    }
    const onFinishFailed = () => {
        message.destroy();
        message.error("登录失败！").then()
    }
    return <Modal title="登录" visible={visible}
        // onOk={onFinish}
                  onCancel={() => {
                      handleOk(false)
                  }}
                  footer={[null, null]}
    >
        <Form
            form={form}
            name="basic"
            labelCol={{
                span: 6,
            }}
            wrapperCol={{
                span: 14,
            }}
            initialValues={{
                remember: false,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            // autoComplete="off"
        >
            <Form.Item
                label="用户名"
                name="username"
                rules={[
                    {
                        required: true,
                        message: '请输入您的用户名称!',
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="密&nbsp;&nbsp;&nbsp;&nbsp;码"
                name="password"
                rules={[
                    {
                        required: true,
                        message: '请输入您的密码!',
                    },
                ]}
            >
                <Input.Password/>
            </Form.Item>
            <Form.Item label="验证码">
                <Input.Group compact>
                    <Form.Item name="code" noStyle>
                        <Input placeholder="验证码" onFocus={() => {
                            getValidateCode();
                        }} style={{width: '50%'}}/>
                    </Form.Item>
                    <Form.Item noStyle>
                        <img src={imageUrl} alt="" onClick={() => {
                            getValidateCode()
                        }} style={{width: '50%'}}/>
                    </Form.Item>
                </Input.Group>
            </Form.Item>
            <Form.Item
                name="remember"
                valuePropName="checked"
                wrapperCol={{
                    offset: 6,
                    span: 14,
                }}
            >
                <Checkbox>记住密码</Checkbox>
            </Form.Item>
            <Form.Item
                wrapperCol={{
                    offset: 3,
                    span: 17
                }}
            >
                <Button type="primary" htmlType="submit" block>
                    登录
                </Button>
            </Form.Item>
        </Form>
    </Modal>
}

//个人中心菜单
export const ProfileMenu = ({active}) => {
    const router = useRouter()
    return <div className="ant-tabs ant-tabs-top ant-tabs-card">
        <ul className="ant-tabs-nav" style={{"transform": "translate(0px, 0px)"}}>
            {Config.PROFILE_MENU.map((m, index) => {
                let cn = "ant-tabs-tab";
                if (m.key === active) {
                    cn += " ant-tabs-tab-active";
                }
                return <li key={index} className={cn}
                           onClick={() => {
                               const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                               router.push(
                                   {
                                       pathname: m.path,
                                       query: {
                                           active: m.key,
                                           page: 1,
                                           pageSize: Config.PAGE_SIZE,
                                           sessionId: sid,
                                       }
                                   }
                               ).then()
                           }}
                >{m.name}</li>
            })}
        </ul>
    </div>;
}