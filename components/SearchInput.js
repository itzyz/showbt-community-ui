import {Select} from 'antd';
import {get} from "../utils/request";
import {Config} from "../utils/config";
import React, {useState} from "react";

const {Option} = Select;

let timeout;
let currentValue;

function fetch(value, callback) {
    if (timeout) {
        clearTimeout(timeout);
        timeout = null;
    }
    currentValue = value;

    function fake() {
        get("/api/searchTopic", {
            pageNo: 1,
            pageSize: Config.PAGE_SIZE,
            title: currentValue
        }).then(d => {
            if (currentValue === value) {
                const result = d.data.data;
                const data = [];
                result.forEach(r => {
                    data.push({
                        value: r.id,
                        text: r.title,
                    });
                });
                callback(data);
            }
        });
    }

    timeout = setTimeout(fake, 1000);
}

export default function SearchInput({value, onChange}) {
    const [data, setData] = useState([])
    const [inputValue, setInputValue] = useState(value)

    const handleSearch = v => {
        if (v) {
            fetch(v, data => setData(data));
        } else {
            setData([])
        }
    };

    const handleChange = v => {
        setInputValue(v)
        onChange(v)
    };

    const options = data.map(d => <Option key={d.value}>{d.text}</Option>);
    return (
        <Select
            showSearch={true}
            value={inputValue}
            defaultActiveFirstOption={false}
            showArrow={false}
            filterOption={false}
            onSearch={handleSearch}
            onChange={handleChange}
            notFoundContent={null}
        >
            {options}
        </Select>
    );
}