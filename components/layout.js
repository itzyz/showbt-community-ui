import Head from 'next/head'
import styles from './layout.module.css'
import Link from 'next/link'

import {Row, Col, Input, Menu, Dropdown, Space} from "antd"
import {DownOutlined} from '@ant-design/icons';
import React, {useEffect, useState} from "react";
import {LoginForm} from "./MyCommonComponents";
import {Config} from "../utils/config";
import {useRouter} from "next/router";

export const siteTitle = '网关管理系统社区'

export default function Layout({children, home}) {
    const layRouter = useRouter()
    const [loginFormVisible, setLoginFormVisible] = useState(false)
    const [userInfo, setUserInfo] = useState(null)
    useEffect(() => {
        const user = sessionStorage.getItem(Config.SESSION_USER_INFO)
        if (user) {
            setUserInfo(user)
        }
    }, [loginFormVisible])
    const handleOk = () => {
        setLoginFormVisible(false);
    };
    return (
        <div style={{minHeight: "100%", marginBottom: "-70px"}}>
            <Head>
                <link rel="icon" href="/favicon.ico"/>
                <meta
                    name="description"
                    content="Learn how to build a personal website using Next.js"
                />
                <meta
                    property="og:image"
                    content={`https://og-image.vercel.app/${encodeURI(
                        siteTitle
                    )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
                />
                <meta name="og:title" content={siteTitle}/>
                <meta name="twitter:card" content="summary_large_image"/>
            </Head>
            <header className={styles.header}>
                <Row>
                    <Col span={12}>
                        <div className={styles.banner}>
                            <div className={styles.logo}>
                                <img src="/images/logo.png"/>
                                <h2>网关管理系统社区</h2>
                            </div>
                            <ul className={styles.menu}>
                                <li>
                                    <a onClick={() => {
                                        const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                        layRouter.push({
                                            pathname: "/", query: {
                                                sessionId: sid
                                            }
                                        }).then()
                                    }}>发现</a>
                                </li>
                                <li>
                                    <a onClick={() => {
                                        const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                        layRouter.push({
                                            pathname: "/topic", query: {
                                                sessionId: sid
                                            }
                                        }).then()
                                    }}>话题</a>
                                </li>
                            </ul>
                            <div className={styles.search}>
                                <Input.Search/>
                            </div>
                        </div>
                    </Col>
                    <Col span={12}>
                        <div className={styles.banner}>
                            <ul className={styles.menu}>
                                {
                                    userInfo ?
                                        <li>
                                            <Dropdown placement="bottomLeft" overlay={<Menu>
                                                <Menu.Item key="1">
                                                    <a onClick={() => {
                                                        const sId = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                                        if (sId) {
                                                            layRouter.push(`/publish?sessionId=${sId}`).then()
                                                        }
                                                    }}>发贴</a>
                                                </Menu.Item>
                                                <Menu.Item key="2">
                                                    <a onClick={() => {
                                                        const sId = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                                        if (sId) {
                                                            layRouter.push(`/profile?active=my_questions&sessionId=${sId}`).then()
                                                        }
                                                    }}>我的主页</a>
                                                </Menu.Item>
                                                <Menu.Item key="3">
                                                    <a onClick={() => {
                                                        sessionStorage.clear();
                                                        layRouter.push("/").then();
                                                    }}>退出</a>
                                                </Menu.Item>
                                            </Menu>}>
                                                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                                    <Space>个人中心<DownOutlined/></Space>
                                                </a>
                                            </Dropdown>
                                        </li>
                                        : <>
                                            <li>
                                                <Link href="#">
                                                    <a onClick={() => {
                                                        setLoginFormVisible(true)
                                                    }}>登录</a>
                                                </Link>
                                                <LoginForm visible={loginFormVisible} handleOk={handleOk}></LoginForm>
                                            </li>
                                            <li>
                                                <Link href="#">
                                                    <a>注册</a>
                                                </Link>
                                            </li>
                                        </>
                                }

                            </ul>
                        </div>
                    </Col>
                </Row>
            </header>
            <main>
                {children}
            </main>
            <div style={{height: "75px"}}></div>
            <footer className={styles.rcFooter}>
                <div className={styles.rcFooterBottomContainer}>@Copy Right by showbt.com</div>
            </footer>
        </div>
    )
}
