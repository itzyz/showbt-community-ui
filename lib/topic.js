import {get} from "../utils/request";
import {Config} from "../utils/config";

export const getTopicList = async (pageNo, pageSize) => {
    let res = await get(Config.WEB_SERVER_HOST + "/community/topic/list", {pageNo, pageSize})
        .then(res => res.data)
        .catch(err => {
            return {
                data: []
            }
        })
    return res;
}

export const getTalkListByTopicId = async (id, pageNo, pageSize, sortBy) => {
    let res = await get(Config.WEB_SERVER_HOST + "/community/topic/talkList", {topicId: id, pageNo, pageSize, sortBy})
        .then(res => res.data)
    return res;
}