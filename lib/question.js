import {configRequestInterceptor, get} from "../utils/request";
import {Config} from "../utils/config";

export async function getQuestionById(sessionId, id) {
    configRequestInterceptor(sessionId ? sessionId : "")
    const result = await get(Config.WEB_SERVER_HOST + `/community/question/getQuestionById/${id}`).then(res => {
        if (res.data.code === 200) {
            return res.data
        } else {
            return {data: []}
        }
    }).catch(err => {
        return {data: []}
    });
    return result;
}

/**
 * 首页问题列表
 * @param pageNo
 * @param pageSize
 * @param sortBy
 * @param search
 * @param tag
 * @param category
 * @returns {Promise<T>}
 */
export async function getHomeQuestionList(pageNo, pageSize, sortBy, search, tag, category = 0) {
    let result = await get(Config.WEB_SERVER_HOST + '/community/question/indexList',
        {pageNo, pageSize, sortBy, search, tag, category}).then(res => {
        if (res.data.code == 200) {
            return res.data;
        } else {
            return null;
        }
    }).catch(err => {
        return null;
    })
    return result;
}

/**
 * 首页右则列表
 * @returns {Promise<T|{data: {userList: [], newQuestions: [], recommend: [], tags: []}}>}
 */
export async function getHomeRightList() {
    let result = await get(Config.WEB_SERVER_HOST + "/community/question/rightList").then(res => {
        if (res.data.code == 200) {
            return res.data
        } else {
            return {
                code: res.data.code,
                data: {
                    userList: [],
                    tags: [],
                    newQuestions: [],
                    recommend: []
                }
            }
        }
    }).catch(err => {
        return {
            data: {
                userList: [],
                tags: [],
                newQuestions: [],
                recommend: []
            }
        }
    });
    return result;
}

/**
 * 我发布的问题
 * @param sessionId
 * @param pageNo
 * @param pageSize
 * @returns {Promise<T>}
 */
export async function getProfileQuestions(sessionId, pageNo, pageSize) {
    configRequestInterceptor(sessionId)
    console.log("getProfileQuestions", sessionId)
    let result = await get(Config.WEB_SERVER_HOST + '/community/profile/myQuestions',
        {pageNo, pageSize}).then(res => {
        console.log(res.data)
        if (res.data.code == 200) {
            return res.data.data;
        } else {
            return null;
        }
    }).catch(err => {
        return null;
    })
    return result;
}

/**
 * 我的收藏
 * @param sessionId
 * @param pageNo
 * @param pageSize
 * @returns {Promise<T>}
 */
export async function getProfileCollects(sessionId, pageNo, pageSize) {
    configRequestInterceptor(sessionId)
    console.log("getProfileCollects", sessionId)
    let result = await get(Config.WEB_SERVER_HOST + '/community/profile/myCollects',
        {pageNo, pageSize}).then(res => {
        if (res.data.code == 200) {
            return res.data.data;
        } else {
            return null;
        }
    }).catch(err => {
        return null;
    })
    return result;
}

/**
 * 我的粉丝
 * @param sessionId
 * @param pageNo
 * @param pageSize
 * @returns {Promise<T>}
 */
export async function getProfileFans(sessionId, pageNo, pageSize) {
    configRequestInterceptor(sessionId)
    console.log("getProfileFans", sessionId)
    let result = await get(Config.WEB_SERVER_HOST + '/community/profile/myFans',
        {pageNo, pageSize}).then(res => {
        if (res.data.code == 200) {
            return res.data.data;
        } else {
            return null;
        }
    }).catch(err => {
        return null;
    })
    return result;
}

/**
 * 我的关注
 * @param sessionId
 * @param pageNo
 * @param pageSize
 * @returns {Promise<T>}
 */
export async function getProfileFollows(sessionId, pageNo, pageSize) {
    configRequestInterceptor(sessionId)
    console.log("getProfileFollows", sessionId)
    let result = await get(Config.WEB_SERVER_HOST + '/community/profile/myFollows',
        {pageNo, pageSize}).then(res => {
        if (res.data.code == 200) {
            return res.data.data;
        } else {
            return null;
        }
    }).catch(err => {
        return null;
    })
    return result;
}