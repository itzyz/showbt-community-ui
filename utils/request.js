import axios from "axios"


/**
 * 跨域解决方法
 * https://www.jianshu.com/p/ac9c58cee838
 * @type {string}
 */
axios.defaults.baseURL = ""
const instance = axios.create({
    baseURL: axios.defaults.baseURL,
    timeout: 15000
})

export const loginInstance = axios.create({
    baseURL: axios.defaults.baseURL,
    timeout: 15000
})


/**
 * 全局请求拦截,发送请求之前
 * 所有请求都需要带token
 */
export const configRequestInterceptor = (sessionId) => {
    instance.interceptors.request.use(function (config) {
        config.headers["taskId"] = "ff8080817be35ff1017be36545f40000";
        if (sessionId){
            config.headers["sbt_community_user_key"] = sessionId;
        }
        return config;
    }, function (error) {
        return Promise.reject(error);
    })
}

configRequestInterceptor()

/**
 * 全局响应拦截， 响应之后拦截
 */
instance.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    return Promise.reject(error);
})

export function get(url, params) {
    return instance.get(url, {
        params
    });
}

export function post(url, data) {
    return instance.post(url, data);
}

export function patch(url, data) {
    return instance.patch(url, data);
}

export function put(url, data) {
    return instance.put(url, data);
}

export function del(url) {
    return instance.delete(url);
}