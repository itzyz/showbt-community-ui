export const Config = {
    WEB_SERVER_HOST:'http://172.18.24.228:9001',
    PAGE_SIZE: 20,
    MENU_DATA_LIST: [
        "综合",
        "提问",
        "分享",
        "讨论",
        "建议",
        "BUG",
        "求职",
        "公告",
        "教程",
        "面试"],
    COLOR_DATA:['#d71345', '#f56a00', '#87d068', '#73b9a2', '#78cdd1', '#90d7ec', '#fde3cf', '#d9d6c3', '#d3d7d4', '#d9d6c3'],
    QUESTION_SORT_LIST:[
        {key:"ALL",name:"全部"},
        {key:"VIEW_HOT",name:"查看最多"},
        {key:"LIKE_HOT",name:"点赞最多"},
        {key:"COMMENT_HOT",name:"评论最多"},
        {key:"WAIT_COMMENT",name:"待回复"},
        {key:"MONTH_HOT",name:"月榜"},
        {key:"WEEK_HOT",name:"周榜"}
    ],
    SESSION_USER_KEY: "user_key",
    SESSION_USER_INFO: "user_info",
    PROFILE_MENU:[
        {key:"my_questions", name:"我的问题", path:"/profile"},
        {key:"my_collects", name:"我的收藏", path:"/profile"},
        {key:"my_notices", name:"我的通知"},
        {key:"my_follows", name:"我的关注"},
        {key:"my_fans", name:"我的粉丝"},
    ]

}