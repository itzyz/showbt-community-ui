import Head from 'next/head'
import Layout, {siteTitle} from '../components/layout'
import {List, Avatar, Col, Row, Card, Pagination} from "antd";
import React from "react";
import {
    TeamOutlined,
    BulbOutlined, LikeFilled
} from '@ant-design/icons';
import {getHomeQuestionList, getHomeRightList} from "../lib/question";
import {useRouter} from 'next/router'
import {Config} from "../utils/config";
import {IconText, QuestionList, TagsShow} from "../components/MyCommonComponents";

export default function Home({indexListData, userList, tags, newQuestions, recommend, query}) {
    const router = useRouter()
    const {sortBy, search, tag, category} = query;
    const pageData = indexListData.data;

    return (
        <Layout home={"home"}>
            <Head>
                <title>{siteTitle}</title>
            </Head>
            {/* menu  start*/}
            <Row>
                <Col span={2}></Col>
                <Col span={20}>
                    <section>
                        <ul className="ant-menu-overflow ant-menu ant-menu-root ant-menu-horizontal ant-menu-light">
                            {
                                Config.MENU_DATA_LIST.map((m, index) => {
                                    let c = category ? category : 0;
                                    let cn = "ant-menu-overflow-item ant-menu-item";
                                    cn = cn + (c == index ? " ant-menu-item-selected" : "")
                                    return <li key={index} className={cn} onClick={() => {
                                        const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                        router.push({
                                            pathname: "/",
                                            query: {
                                                page: 1,
                                                pageSize: Config.PAGE_SIZE,
                                                category: index,
                                                sessionId: sid
                                            }
                                        }).then()
                                    }}>{m}</li>
                                })
                            }
                        </ul>
                    </section>
                </Col>
                <Col span={2}></Col>
            </Row>
            {/*menu  end*/}
            <Row>
                <Col span={2}></Col>
                {/*question list begin*/}
                <Col span={14}>
                    <Card className="card-container" style={{borderTop: "0"}}>
                        <div className="ant-tabs ant-tabs-top ant-tabs-card">
                            <ul className="ant-tabs-nav" style={{"transform": "translate(0px, 0px)"}}>
                                {Config.QUESTION_SORT_LIST.map((m, index) => {
                                    let cn = "ant-tabs-tab";
                                    let sb = sortBy ? sortBy : "ALL";
                                    cn = cn + (sb == m.key ? " ant-tabs-tab-active" : "")
                                    return <li key={index} className={cn}
                                               onClick={() => {
                                                   const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                                   router.push(
                                                       {
                                                           pathname: "/",
                                                           query: {
                                                               page: 1,
                                                               pageSize: Config.PAGE_SIZE,
                                                               category: category,
                                                               sortBy: m.key,
                                                               sessionId: sid
                                                           }
                                                       }
                                                   ).then()
                                               }}
                                    >{m.name}</li>
                                })}
                            </ul>
                        </div>
                        <QuestionList data={pageData} router={router}/>
                        <div>
                            <Pagination defaultCurrent={1} total={pageData.total}
                                        current={pageData.current}
                                        pageSize={Config.PAGE_SIZE}
                                        onChange={(p, ps) => {
                                            const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                            router.push({
                                                pathname: "/",
                                                query: {
                                                    page: p,
                                                    pageSize: ps ? ps : Config.PAGE_SIZE,
                                                    sortBy,
                                                    search,
                                                    tag,
                                                    category,
                                                    sessionId: sid
                                                }
                                            }).then()
                                        }}
                            />
                        </div>
                    </Card>
                </Col>
                {/*question list end*/}

                <Col span={6}>
                    <TagsShow title="最热标签" data={tags}/>
                    <Card title={<IconText icon={() => <TeamOutlined style={{fontSize: "20px"}}/>} text="最近登入"/>}
                          bordered={false}>
                        <List
                            size="small"
                            itemLayout="horizontal"
                            dataSource={userList}
                            renderItem={item => (
                                <List.Item>
                                    <List.Item.Meta
                                        avatar={<Avatar
                                            src={item['avatarUrl']}/>}
                                        title={<a href="#">{item.name}</a>}
                                    />
                                    <div>问：({item['questionCount']}) 粉：({item['fansCount']})</div>
                                </List.Item>
                            )}
                        />
                    </Card>
                    <Card title={<IconText icon={() => <LikeFilled style={{color: "#d71345", fontSize: "20px"}}/>}
                                           text="热门推荐"/>} bordered={false}>
                        <List
                            size="small"
                            itemLayout="horizontal"
                            dataSource={recommend}
                            renderItem={(item, index) => (
                                <List.Item>
                                    <List.Item.Meta
                                        avatar={<Avatar shape="square" style={{
                                            color: '#ffffff',
                                            backgroundColor: Config.COLOR_DATA[index]
                                        }}>{index + 1}</Avatar>}
                                        title={<a
                                            onClick={() => {
                                                const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                                router.push({
                                                    pathname: "/question/" + item.id,
                                                    query: {
                                                        sessionId: sid
                                                    }
                                                }).then()
                                            }}
                                        >{item.title}</a>}
                                    />
                                </List.Item>
                            )}
                        />
                    </Card>
                    <Card title={<IconText icon={() => <BulbOutlined style={{color: "#225a1f", fontSize: "20px"}}/>}
                                           text="最近更新"/>} bordered={false}>
                        <List
                            size="small"
                            itemLayout="horizontal"
                            dataSource={newQuestions}
                            renderItem={item => (
                                <List.Item>
                                    <List.Item.Meta
                                        // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                        title={<a
                                            onClick={()=>{
                                                const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                                router.push({
                                                    pathname: "/question/" + item.id,
                                                    query: {
                                                        sessionId: sid
                                                    }
                                                }).then()
                                            }}
                                            // href={"/question/" + item.id}
                                        >{item.title}</a>}
                                    />
                                </List.Item>
                            )}
                        />
                    </Card>
                </Col>
                <Col span={2}></Col>
            </Row>
        </Layout>
    )
}

export async function getServerSideProps(context) {
    const {page, pageSize, sortBy, search, tag, category} = context.query;
    const allQuestionsData = await getHomeQuestionList(
        page ? page : 1,
        pageSize ? pageSize : Config.PAGE_SIZE,
        sortBy ? sortBy : "ALL",
        search,
        tag,
        category ? category : 0)
    const rightListResult = await getHomeRightList();
    const {userList, tags, newQuestions, recommend} = rightListResult.data;
    //here,you can fetch data by context.query
    return {
        props: {
            query: context.query,
            indexListData: allQuestionsData,
            userList,
            tags,
            newQuestions,
            recommend
        }, // will be passed to the page component as props
    }
}