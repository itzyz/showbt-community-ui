import Layout, {siteTitle} from "../../components/layout";
import React from "react";
import {getTalkListByTopicId} from "../../lib/topic";
import Head from "next/head";
import {Card, Col, Pagination, Row, Tag} from "antd";
import {CollectionUsers, IconText, QuestionList} from "../../components/MyCommonComponents";
import {CommentOutlined} from "@ant-design/icons";
import {useRouter} from "next/router";
import {Config} from "../../utils/config";
import utilStyles from "../../styles/utils.module.css";

export default function QuestionListForTopic({talkList, query}) {
    const router = useRouter()
    const {questionList, userList, relatedTopics} = talkList.data
    return <Layout home={"question list for topic"}>
        <Head>
            <title>{siteTitle}</title>
        </Head>
        <Row>
            <Col span={2}></Col>
            {/*question list begin*/}
            <Col span={14}>
                <QuestionList data={questionList} router={router}/>
                <div>
                    <Pagination defaultCurrent={1} total={questionList.total}
                                current={questionList.current}
                                onChange={(p, ps) => {
                                    const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                    router.push({
                                        pathname: "/topic/" + query.id,
                                        query: {pageNo: p, sessionId: sid}
                                    }).then()
                                }}
                    />
                </div>
            </Col>
            <Col span={6}>
                <Card title={<IconText icon={() => <CommentOutlined style={{color: "#f40", fontSize: "20px"}}/>}
                                       text="相关话题"/>} bordered={false}>
                    {relatedTopics ? <ul className={utilStyles.tags}>
                        {relatedTopics.map((r, index) => {
                            return <li key={index}>
                                <a onClick={() => {
                                    const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                    router.push({
                                        pathname: "/topic/" + r.id,
                                        query: {sessionId: sid}
                                    }).then()
                                }}><Tag>{r.title}</Tag></a>
                            </li>
                        })}
                    </ul> : null}
                </Card>
                <CollectionUsers data={userList}/>
            </Col>
            <Col span={2}></Col>
        </Row>
    </Layout>
}

export async function getServerSideProps(context) {
    const {query} = context
    const result = await getTalkListByTopicId(query.id, query.pageNo, Config.PAGE_SIZE, query.sortBy)
    return {
        props: {
            query: context.query,
            talkList: result
        }
    }
}