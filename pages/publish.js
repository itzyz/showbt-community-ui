import Layout from "../components/layout";
import {Card, Row, Col, Form, Input, Select, Space, Button} from "antd";
import ReactMde from "react-mde";
import React, {useState} from "react";
import * as Showdown from "showdown";
import "react-mde/lib/styles/css/react-mde-all.css";
import {Config} from "../utils/config";
import SearchInput from "../components/SearchInput";
import {post} from "../utils/request";
import {useRouter} from "next/router";

const {Option} = Select
export default function PublicForm({}) {
    const router = useRouter()
    const [form] = Form.useForm()
    const converter = new Showdown.Converter({
        tables: true,
        simplifiedAutoLink: true,
        strikethrough: true,
        tasklists: true,
        extensions: []
    });
    const onFinish = (values) => {
        const userKey = sessionStorage.getItem("user_key")
        values.userKey = userKey
        post("/api/publish", values).then(r => {
            router.push("/").then()
        })
    }
    const [selectedTab, setSelectedTab] = useState("write");
    const onChange = (value) => {
        // console.log(value,"  ddddddddddd")
    }
    return <Layout>
        <Row>
            <Col span={2}></Col>
            <Col span={20}>
                <Card>
                    <Form form={form} onFinish={onFinish}>
                        <Form.Item name="title" label="标题" rules={[{required: true, message: "请输入标题"}]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item name="category" label="分类" rules={[{required: true, message: "请输入标题"}]}>
                            <Select style={{width: 200}}
                                    placeholder="请选择分类">
                                {
                                    Config.MENU_DATA_LIST.map((m, index) => {
                                        return <Option key={index}>{m}</Option>
                                    })
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item name="content" label="内容" rules={[{required: true, message: "请输入内容"}]}>
                            <ReactMde
                                // value={contentValue}
                                // onChange={setContentValue}
                                selectedTab={selectedTab}
                                onTabChange={setSelectedTab}
                                generateMarkdownPreview={markdown =>
                                    Promise.resolve(converter.makeHtml(markdown))
                                }
                                l18n={
                                    {
                                        write: '输入',
                                        preview: '预览',
                                    }
                                }
                            />
                        </Form.Item>
                        <Form.Item name="tags" label="标签" rules={[{required: true, message: "请输入标签"}]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item name="topic" label="话题">
                            <SearchInput onChange={onChange}/>
                        </Form.Item>
                        <Form.Item wrapperCol={{span: 12, offset: 6}}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </Card>
            </Col>
            <Col span={2}></Col>
        </Row>
    </Layout>
}