import '../styles/global.css'
import 'antd/dist/antd.css'
import React from "react";

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
