import {Config} from "../../utils/config";
import axios from "axios";

export default (req, res) => {
    const {title, content, category, id, tags, topic, userKey} = req.body
    console.log("userKey::::", userKey)
    axios.post(`${Config.WEB_SERVER_HOST}/community/question/publish`, {
        title, description: content, category, id, tag: tags, topic
    }, {
        headers: {
            "taskId": "ff8080817be35ff1017be36545f40000",
            "sbt_community_user_key": userKey
        }
    }).then(r => {
        res.status(200).json(r.data)
    }).catch(e => {
        res.status(201).json({code: 500, msg: "发布服务异常"})
    })
}