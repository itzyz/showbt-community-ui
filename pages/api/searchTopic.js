import {get} from "../../utils/request";
import {Config} from "../../utils/config";

export default (req, res) => {
    get(`${Config.WEB_SERVER_HOST}/community/topic/getTopicPage`, {
        pageNo: 1,
        pageSize: Config.PAGE_SIZE,
        title:req.query.title
    }).then(r => {
        res.status(200).json(r.data)
    }).catch(err => {
        res.status(200).json({code: 500, msg: "获取数据出错", data: []})
    })
}
