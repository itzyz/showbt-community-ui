import {get} from "../../utils/request";
import {Config} from "../../utils/config";

export default (req, res) => {
    get(Config.WEB_SERVER_HOST + "/community/user/verifyCode").then(r => {
        res.status(200).json(r.data)
    }).catch(err => {
        console.log(err)
    })
}