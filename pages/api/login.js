import {post} from "../../utils/request";
import {Config} from "../../utils/config";
import Querystring from "query-string"

export default (req, res) => {
    const {username, password, code, randomCode} = req.body
    let data = Querystring.stringify({username, password, code, randomCode});
    post(Config.WEB_SERVER_HOST + "/community/user/login", data).then(r => {
        res.status(200).json(r.data)
    }).catch(err => {
        res.status(500).json({data: "error"})
    })
}