import Layout from '../../components/layout'
import Head from 'next/head'
import utilStyles from '../../styles/utils.module.css'
import {getQuestionById} from "../../lib/question";
import {Divider, Col, Row, PageHeader, Card, Avatar, List, Comment, Tooltip, Space, Form, Input, Button} from "antd";
import React, {createElement} from "react";
import {Config} from "../../utils/config";
import {
    LikeFilled, TagsFilled, HeartFilled, MessageFilled, ShareAltOutlined, LikeOutlined
} from '@ant-design/icons';
import {CollectionUsers, IconText, LongToDate} from "../../components/MyCommonComponents";
import * as Showdown from "showdown";
import {useRouter} from "next/router";
// import "react-mde/lib/styles/css/react-mde-all.css";


const {TextArea} = Input;

export default function QuestionDetail({postData}) {
    const router = useRouter()
    const question = postData.data['question'];
    const relatedQuestions = postData.data['relatedQuestions'] ? postData.data['relatedQuestions'] : []
    const collectUsers = postData.data['collect_users'] ? postData.data['collect_users'] : []
    const comments = postData.data['comments'] ? postData.data['comments'] : []
    const converter = new Showdown.Converter({
        tables: true,
        simplifiedAutoLink: true,
        strikethrough: true,
        tasklists: true
    });


    const Editor = ({onChange, onSubmit, submitting, value}) => (
        <>
            <Form.Item>
                <TextArea rows={4} onChange={onChange} value={value} disabled={true} placeholder="请登录后再发表评论！"/>
            </Form.Item>
            <Form.Item>
                <Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary" disabled={true}>
                    评论
                </Button>
            </Form.Item>
        </>
    );

    return (
        <Layout>
            <Head>
                <title>{question ? question.title : "详情页"}</title>
            </Head>
            {question ?
                <article className={utilStyles.article}>
                    <Row>
                        <Col span={2}></Col>
                        <Col span={16}>
                            <Card style={{marginRight: "10px", minHeight: "800px"}}>
                                <PageHeader
                                    className="site-page-header"
                                    title={question.title}
                                    ghost={false}
                                    onBack={() => window.history.back()}
                                >
                                    <ul className={utilStyles.tags}>
                                        <li>类型：{Config.MENU_DATA_LIST[question.category]}</li>
                                        <li>作者：{question.user.name}</li>
                                        <li><Divider type="vertical"/>发布时间：{LongToDate(question['gmtCreate'])}</li>
                                        <li><Divider type="vertical"/> 预计阅读时长：{question.showTime}</li>
                                        <li><Divider type="vertical"/> 阅读数量：{question['viewCount']}</li>
                                    </ul>
                                </PageHeader>
                                <Divider/>
                                <div dangerouslySetInnerHTML={{__html: converter.makeHtml(question.description)}}/>

                                {/*<ReactMarkdown className="markdown" children={question.description} />*/}

                                <Divider/>
                                <ul>
                                    <li><a href="#" key="it_btn_share"><IconText icon={ShareAltOutlined} text="分享"/></a>
                                    </li>
                                    <li><a href="#" key="it_btn_heart"><IconText icon={HeartFilled} text="收藏"/></a></li>
                                    <li><a href="#" key="it_btn_like"><IconText icon={LikeFilled} text="点赞"/></a></li>
                                </ul>
                                <br/>
                                <Divider/>
                                <ul>
                                    {question['tag'].split(',').map((t, index) => {
                                        return <li key={index}><IconText icon={TagsFilled} text={t}/></li>
                                    })}
                                </ul>
                                <Divider/>
                                <List
                                    header={<div>评论人数：{comments.length}人</div>}
                                    itemLayout="horizontal"
                                    dataSource={comments}
                                    renderItem={item => (
                                        <Comment
                                            actions={[
                                                <Tooltip key="comment-basic-like" title="Like">
                                                <span onClick={() => {
                                                    console.log(item.id)
                                                }}>
                                                    <Space>
                                                        {createElement(LikeFilled)}
                                                        <span className="comment-action">{item['likeCount']}</span>
                                                    </Space>
                                                </span>
                                                </Tooltip>,
                                                <Tooltip key="comment-basic-comment" title="comment">
                                                <span onClick={() => {
                                                    console.log(item.id)
                                                }}>
                                                    <Space>
                                                        {createElement(MessageFilled)}
                                                        <span className="comment-action">{item['commentCount']}</span>
                                                    </Space>
                                                </span>
                                                </Tooltip>
                                            ]
                                            }
                                            author={<a>{item.user['name']}</a>}
                                            avatar={
                                                <Avatar
                                                    src={item.user['avatarUrl']}
                                                    alt={item.user['name']}
                                                />
                                            }
                                            content={
                                                <p>
                                                    {item.content}
                                                </p>
                                            }
                                            datetime={
                                                item.showTime
                                            }
                                        />
                                    )}
                                />
                                <Divider/>
                                <Editor/>
                            </Card>
                        </Col>
                        <Col span={4}>
                            <Card bordered={false}
                                  title="发起人"
                            >
                                <Card.Meta
                                    avatar={
                                        <Avatar shape="square" src={question.user['avatarUrl']}/>
                                    }
                                    title={question.user['name']}
                                    description={question.user['bio']}
                                    style={{marginBottom: "10px"}}
                                />
                                <p>公司：{question.user['company']}</p>
                                <p>所在地：{question.user['location']}</p>
                                <p>积分：{question.user['rank'] ? question.user['rank'] : 0}</p>
                            </Card>
                            <Card title="相关问题" bordered={false}>
                                <List
                                    itemLayout="horizontal"
                                    dataSource={relatedQuestions}
                                    renderItem={item => (
                                        <List.Item.Meta
                                            // avatar={<Avatar src={item.user['avatarUrl']}/>}
                                            title={<a
                                                onClick={() => {
                                                    const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                                    router.push({
                                                        pathname: "/question/" + item.id,
                                                        query: {
                                                            sessionId: sid
                                                        }
                                                    }).then()
                                                }}
                                            >{item.title}</a>}
                                        />
                                    )}
                                />
                            </Card>
                            <CollectionUsers data={collectUsers}/>
                        </Col>
                        <Col span={2}></Col>
                    </Row>
                </article> : <h1>没找到内容！！！！！</h1>
            }
        </Layout>
    )
}

export async function getServerSideProps(context) {
    const {query} = context;
    const res = await getQuestionById(query.sessionId, query.id)
    return {
        props: {
            query: context.query,
            postData: res
        }
    }
}