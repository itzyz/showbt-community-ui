import Layout, {siteTitle} from "../components/layout";
import Head from "next/head";
import React from "react";
import {Card, Col, Row, Skeleton, Avatar, List, Button, Space, Pagination} from "antd";
import {getTopicList} from "../lib/topic";
import {useRouter} from "next/router";
import {Config} from "../utils/config";

const {Meta} = Card;

export default function Topic({query, topicList}) {
    const {pageSize} = query
    const router = useRouter()

    return <Layout home={"topic"}>
        <Head>
            <title>{siteTitle}</title>
        </Head>
        <Row>
            <Col span={2}></Col>
            {/*topic list begin*/}
            <Col span={20}>
                <List
                    grid={{gutter: 16, column: 5}}
                    dataSource={topicList['records']}
                    renderItem={item => (
                        <List.Item>
                            <Card>
                                <Meta
                                    avatar={
                                        <Avatar src={item['image']}/>
                                    }
                                    title={<a onClick={() => {
                                        const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                        router.push({
                                            pathname: "/topic/" + item.id,
                                            query: {sessionId: sid}
                                        }).then(r => r)
                                    }}>{item.title}</a>}
                                />
                                <div style={{height: "100px", marginTop: "10px"}}>{item['simpleDesc']}</div>
                                <div><Space>
                                    <Button size="small">关注</Button>
                                    <span>{item['followCount']}人关注</span>
                                    <span>{item['talkCount']}个讨论</span>
                                </Space></div>
                            </Card>
                        </List.Item>
                    )}
                />
                <div>
                    <Pagination defaultCurrent={1} total={topicList.total}
                                current={topicList.current}
                                pageSize={pageSize ? pageSize : Config.PAGE_SIZE}
                                onChange={(p, ps) => {
                                    const sid = sessionStorage.getItem(Config.SESSION_USER_KEY)
                                    router.push({
                                        pathname: "/topic",
                                        query: {
                                            page: p,
                                            pageSize: ps ? ps : Config.PAGE_SIZE,
                                            sessionId: sid
                                        }
                                    }).then()
                                }}
                    />
                </div>
            </Col>
            <Col span={2}></Col>
        </Row>
    </Layout>
}

export async function getServerSideProps(context) {
    const {page, pageSize} = context.query;
    let topicList = await getTopicList(page ? page : 1,
        pageSize ? pageSize : Config.PAGE_SIZE,);
    return {
        props: {
            query: context.query,
            topicList: topicList.data,
        }, // will be passed to the page component as props
    }
}