/**
 * 我的主页
 * @constructor
 */
import Layout, {siteTitle} from "../components/layout";
import React from "react";
import Head from "next/head";
import {Card, Col, Row} from "antd";
import {getProfileCollects, getProfileFans, getProfileFollows, getProfileQuestions} from "../lib/question";
import {CollectionUsers, IconText, ProfileMenu, QuestionList} from "../components/MyCommonComponents";
import {useRouter} from "next/router";

export default function Profile({data, userData, query}) {
    const router = useRouter()
    const {active, sessionId} = query;
    return <Layout home={"我的和页"}>
        <Head>
            <title>{siteTitle + "-我的主页"}</title>
        </Head>
        <Row>
            <Col span={2}></Col>
            <Col span={14}>
                <Card className="card-container" style={{borderTop: "0"}}>
                    <ProfileMenu active={active ? active : "my_questions"}/>
                    {data ? <QuestionList data={data} router={router}/> : null}
                    {userData ? <CollectionUsers data={userData}/> : null}
                </Card>

            </Col>
            <Col span={6}>
                asdfasdf
            </Col>
            <Col span={2}></Col>
        </Row>
    </Layout>
}

export async function getServerSideProps(context) {
    const {sessionId, active} = context.query;
    let data = null;
    if (active === "my_questions") {
        data = await getProfileQuestions(sessionId, 1, 15)
    }
    if (active === "my_collects") {
        data = await getProfileCollects(sessionId, 1, 15);
    }
    let userData = null;
    if (active === "my_follows") {
        userData = await getProfileFollows(sessionId, 1, 15);
    }
    if (active === "my_fans") {
        userData = await getProfileFans(sessionId, 1, 15);
    }
    return {
        props: {
            query: context.query,
            data: data,
            userData: userData,
        }, // will be passed to the page component as props
    }
}